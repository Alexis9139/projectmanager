﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using MediatR;

namespace EscuelaNet.Aplicacion.Conocimientos.Commands.CategoriaCommand
{
    public class UpdateCategoriaCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
