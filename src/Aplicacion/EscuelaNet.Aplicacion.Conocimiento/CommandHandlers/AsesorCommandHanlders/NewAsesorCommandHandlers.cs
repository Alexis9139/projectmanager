﻿using EscuelaNet.Aplicacion.Conocimiento.Command.AsesorCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.AsesorCommandHanlders
{
    public class NewAsesorCommandHandlers : IRequestHandler<NewAsesorCommand, CommandRespond>
    {
        private IAsesorRepository _asesorrepositorio;
        public NewAsesorCommandHandlers (IAsesorRepository asesorrepositorio)
        {
            _asesorrepositorio = asesorrepositorio;
        }
        public Task<CommandRespond> Handle(NewAsesorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Nombre))
            {
                try
                {
                    var asesor = new Asesor(request.Nombre, request.Apellido, request.Idioma, request.Pais);
                    _asesorrepositorio.Add(asesor);
                    _asesorrepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);

                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos.";
                return Task.FromResult(responde);
            }
        }
    }
}
