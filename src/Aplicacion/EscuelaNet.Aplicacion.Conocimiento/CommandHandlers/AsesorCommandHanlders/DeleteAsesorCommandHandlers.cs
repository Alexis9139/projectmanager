﻿using EscuelaNet.Aplicacion.Conocimiento.Command.AsesorCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.AsesorCommandHanlders
{
    public class DeleteAsesorCommandHandlers : IRequestHandler<DeleteAsesorCommand, CommandRespond>
    {
        private IAsesorRepository _asesorrepositorio;
        public DeleteAsesorCommandHandlers(IAsesorRepository asesorrepositorio)
        {
            _asesorrepositorio = asesorrepositorio;
        }
        public Task<CommandRespond> Handle(DeleteAsesorCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var asesor = _asesorrepositorio.GetAsesor(request.IDAsesor);
                _asesorrepositorio.Delete(asesor);
                _asesorrepositorio.UnitOfWork.SaveChanges();
                responde.Succes = true;
                return Task.FromResult(responde);

            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = ex.Message;
                return Task.FromResult(responde);
            }
        }
    }
}
