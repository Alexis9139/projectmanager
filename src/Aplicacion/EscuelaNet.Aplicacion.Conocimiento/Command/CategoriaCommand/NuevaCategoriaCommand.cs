﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using MediatR;

namespace EscuelaNet.Aplicacion.Conocimiento.Commands.CategoriaCommand
{
    public class NuevaCategoriaCommand : IRequest<CommandRespond>
    {
        public string Nombre { get; set; }
        public string  Descripcion { get; set; }
        public int Id { get; set; }
    }
}
