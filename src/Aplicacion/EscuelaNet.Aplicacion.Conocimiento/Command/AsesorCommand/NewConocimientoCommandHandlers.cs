﻿using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.Command.AsesorCommand
{
    public class NewConocimientoCommand : IRequest<CommandRespond>
    {
        public int IDConocimiento { get; set; }

        public int IDAsesor { get; set; }

        public int IDCategoria { get; set; }
        public string Nombre { get; set; }
        public Demanda Demanda { get; set; }
    }
}
