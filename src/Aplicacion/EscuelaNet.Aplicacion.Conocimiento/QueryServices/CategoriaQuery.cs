﻿using Dapper;
using EscuelaNet.Aplicacion.Conocimiento.QueryModels;
using EscuelaNet.Aplicacion.Programadores.QueryServices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryServices
{

    public class CategoriaQuery : ICategoriaQuery
    {
        private string _connectionString;

        public CategoriaQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public CategoriaQueryModel GetCategoria(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<CategoriaQueryModel>(
                    @"  SELECT c.IDCategoria as Id, c.Nombre as Nombre,
                    c.Descripcion as Descripcion
                    FROM Categoria as c
                    WHERE IDCategoria = @id ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<CategoriaQueryModel> ListCategoria()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<CategoriaQueryModel>(
                    @"SELECT c.IDCategoria as Id, 
                    c.Nombre as Nombre,
                    c.Apellido as Apellido,
                    FROM Categorias as c
                    "
                    ).ToList();
            }


        }
    }
}
