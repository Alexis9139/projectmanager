﻿using EscuelaNet.Aplicacion.Proyectos.Responds;
using EscuelaNet.Dominio.Proyectos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.Commands.LineaCommand
{
    public class NuevaLineaCommand : IRequest<CommandRespond>
    {
        public string Nombre { get; set; }
        public IList<Proyecto> Proyectos { get; set; }
    }
}
