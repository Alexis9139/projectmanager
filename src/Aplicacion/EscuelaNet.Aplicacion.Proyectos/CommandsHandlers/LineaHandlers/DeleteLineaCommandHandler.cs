﻿using EscuelaNet.Aplicacion.Proyectos.Commands.LineaCommand;
using EscuelaNet.Aplicacion.Proyectos.Responds;
using EscuelaNet.Dominio.Proyectos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.CommandsHandlers.LineaHandlers
{
    public class DeleteLineaCommandHandler  : IRequestHandler<DeleteLineaCommand, CommandRespond>
    {
        private ILineaRepository _lineaRepository;
        public DeleteLineaCommandHandler(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }

        public Task<CommandRespond> Handle(DeleteLineaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            try
            {
                var linea = _lineaRepository.GetLinea(request.ID);
                _lineaRepository.Delete(linea);
                _lineaRepository.UnitOfWork.SaveChanges();

                responde.Success = true;
                return Task.FromResult(responde);
            }
            catch(Exception ex)
            {
                responde.Success = false;
                responde.Error = "Error al borrar linea de Producción";
                return Task.FromResult(responde);
            }
        }
    }
}
