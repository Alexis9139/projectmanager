﻿using EscuelaNet.Aplicacion.Proyectos.Commands.LineaCommand;
using EscuelaNet.Aplicacion.Proyectos.Responds;
using EscuelaNet.Dominio.Proyectos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Proyectos.CommandsHandlers.LineaHandlers
{
    public class NuevaLineaCommandHandler : IRequestHandler<NuevaLineaCommand, CommandRespond>
    {
        private ILineaRepository _lineaRepository;
        public NuevaLineaCommandHandler(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }

        public Task<CommandRespond> Handle(NuevaLineaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (string.IsNullOrEmpty(request.Nombre))
            {
                responde.Success = false;
                responde.Error = "La linea debe tener un Nombre";
                return Task.FromResult(responde);
            }
            else
            {
                _lineaRepository.Add(new LineaDeProduccion(request.Nombre));
                _lineaRepository.UnitOfWork.SaveChanges();

                responde.Success = true;
                return Task.FromResult(responde);
            }
        }
    }
}
