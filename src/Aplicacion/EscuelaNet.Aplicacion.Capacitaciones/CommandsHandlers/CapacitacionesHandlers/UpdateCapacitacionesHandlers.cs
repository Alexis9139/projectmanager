﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.CapacitacionesHandlers
{
    public class UpdateCapacitacionesHandlers : IRequestHandler<UpdateCapacitacionCommand, CommandRespond>
    {
        private ICapacitacionRepository _capacitacionRepository;

        public UpdateCapacitacionesHandlers(ICapacitacionRepository capacitacionRepository)
        {
            _capacitacionRepository = capacitacionRepository;
        }
        public Task<CommandRespond> Handle(UpdateCapacitacionCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();
            if (request.Duracion > 0 && request.Minimo > 0 && request.Maximo > 0
                && request.Precio >= 0)
            {
                var capa = _capacitacionRepository.GetCapacitacion(request.Id);
                capa.Duracion = request.Duracion;
                capa.Maximo = request.Maximo;
                capa.Minimo = request.Minimo;
                capa.Estado = request.Estado;
                capa.Fin = request.Fin;
                capa.Inicio = request.Inicio;
                capa.IDLugar = request.IDLugar;
                capa.Precio = request.Precio;

                _capacitacionRepository.Update(capa);

                _capacitacionRepository.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);

            }
            else
            {
                responde.Succes = false;
                responde.Error = "Error Campos en 0";
                return Task.FromResult(responde);
            }
        }
    }
}
