﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public interface IEquipoRepository : IRepository<Equipo>
    {
        Equipo Add(Equipo equipo);
        void Update(Equipo equipo);
        void Delete(Equipo equipo);
        void DeleteProgramador(Programador programador);
        Equipo GetEquipo(int id);
        List<Equipo> ListEquipo();
    }
}
