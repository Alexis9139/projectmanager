﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
   public class Categoria : Entity, IAggregateRoot

    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }

        //public IList<Asesor> Asesores { get; set; }
        public Categoria(string nombre, string descripcion)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Descripcion = descripcion;            
        }
        private Categoria()
        {
        }

  
       public void AgregarConocimiento(Conocimiento conocimiento)
        {
            if (Conocimientos == null)
            {
                Conocimientos = new List<Conocimiento>();
            }
            this.Conocimientos.Add(conocimiento);

        }

        //public void AgregarAsesor(Asesor asesor)
        //{
        //    if (this.Asesores == null)
        //    {
        //        this.Asesores = new List<Asesor>();
        //    }
        //    if (!Asesores.Contains(asesor))
        //    {
        //        this.Asesores.Add(asesor);
        //    }
        //    else
        //    {
        //        throw new ExcepcionDeCategoria("Ya esta el Asesor");
        //    }
        //}
    }
}
