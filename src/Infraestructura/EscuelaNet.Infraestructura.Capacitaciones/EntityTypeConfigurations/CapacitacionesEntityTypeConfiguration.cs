﻿using System.Data.Entity.ModelConfiguration;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Infraestructura.Capacitaciones
{
    public class CapacitacionesEntityTypeConfiguration : EntityTypeConfiguration<Dominio.Capacitaciones.Capacitacion>
    {
        public CapacitacionesEntityTypeConfiguration()
        {
            this.ToTable("Capacitaciones");
            this.HasKey<int>(i => i.ID);
            this.Property(i => i.ID)
                .HasColumnName("IDCapacitacion");
            this.Property(x => x.IDLugar)
                .IsRequired();
            this.Property(c => c.Minimo)
                .IsRequired();
            this.Property(c => c.Maximo)
                .IsRequired();
            this.Property(c => c.Duracion)
                .IsRequired();
            this.Property(c => c.Precio)
                .IsRequired();
            this.Property(c => c.Inicio)
                .IsRequired();
            this.Property(c => c.Fin)
                .IsRequired();
            this.HasMany<Tema>(i => i.Temas)
                .WithMany(t => t.Capacitaciones)
                .Map(it => {
                    it.MapLeftKey("IDCapacitacion");
                    it.MapRightKey("IDTema");
                    it.ToTable("CapacitacionTema");
                });

        }
    }
}