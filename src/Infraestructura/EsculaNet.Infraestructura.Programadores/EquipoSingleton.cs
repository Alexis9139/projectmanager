﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Programadores.Web.Infraestructura
{
    public sealed class EquipoSingleton : IUnitOfWork
    {
        private static EquipoSingleton _instancia = new EquipoSingleton();

        public List<Equipo> Equipos { get; set; }


        private EquipoSingleton()
        {

            //asdasd
            this.Equipos = new List<Equipo>();

            Equipos.Add(new Equipo("Tiburones", "Argentina", 10));

            var programador = new Programador("Juan", "Gomez", "1", "27.000.101", "Lider", new DateTime(1980, 1, 1));
            var conocimiento = new Skills(".Net", Experiencia.Junior);
            programador.PushConocimiento(conocimiento);

            var programador1 = new Programador("Pedro", "Gomez", "1", "27.000.104", "Programador", new DateTime(1980, 1, 1));
            var conocimiento1 = new Skills("Python", Experiencia.Senior);
            programador1.PushConocimiento(conocimiento1);

            var conocimientoequipo = new Skills(".Net", Experiencia.Junior);
            this.Equipos[0].PushSkill(conocimientoequipo);
            this.Equipos[0].PushProgramador(programador);
            this.Equipos[0].PushProgramador(programador1);
            //====================================================================================//
            Equipos.Add(new Equipo("Comadreja", "España", 12));

            var programador2 = new Programador("Juan", "Perez", "1", "27.000.102", "Lider", new DateTime(1980, 1, 1));
            var conocimiento2 = new Skills("Java", Experiencia.Senior);
            programador2.PushConocimiento(conocimiento2);

            var programador3 = new Programador("Pedro", "Perez", "1", "27.000.105", "Programador", new DateTime(1980, 1, 1));
            var conocimiento3 = new Skills("Java", Experiencia.Advanced);
            programador3.PushConocimiento(conocimiento3);

            var conocimientoequipo1 = new Skills("Java", Experiencia.Junior);
            this.Equipos[1].PushSkill(conocimientoequipo1);
            this.Equipos[1].PushProgramador(programador2);
            this.Equipos[1].PushProgramador(programador3);
            //====================================================================================//
            Equipos.Add(new Equipo("Camaleones", "Alemania", -10));

            var programador4 = new Programador("Juan", "Albarez", "1", "27.000.103", "Lider", new DateTime(1980, 1, 1));
            var conocimiento4 = new Skills("Python", Experiencia.Senior);
            programador4.PushConocimiento(conocimiento4);

            var programador5 = new Programador("Pedro", "Albarez", "1", "27.000.106", "Programador", new DateTime(1980, 1, 1));
            var conocimiento5 = new Skills("C#", Experiencia.Junior);
            programador5.PushConocimiento(conocimiento5);

            var conocimientoequipo2 = new Skills("C#", Experiencia.SemiSenior);
            this.Equipos[2].PushSkill(conocimientoequipo2);
            this.Equipos[2].PushProgramador(programador4);
            this.Equipos[2].PushProgramador(programador5);

        }

        public static EquipoSingleton Instancia
        {
            get
            {
                return _instancia;
            }
        }

        public int SaveChanges()
        {
           return 1;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}