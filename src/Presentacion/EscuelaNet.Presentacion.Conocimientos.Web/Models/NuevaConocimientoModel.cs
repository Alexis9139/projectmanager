﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class NuevaConocimientoModel
    {
        public string Titulo { get; set; }
        public int IdCate { get; set; }
        public int IdConocimiento { get; set; }
        public string Nombre { get; set; }
        public Demanda Demanda { get; set; }
    }
}