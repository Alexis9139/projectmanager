﻿//using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Conocimientos.Repositorios;
using EscuelaNet.Aplicacion.Conocimiento.QueryServices;
using System.Threading.Tasks;
using MediatR;
using EscuelaNet.Aplicacion.Conocimiento.Commands.CategoriaCommand;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Controllers
{
    public class CategoriasController : Controller
    {
        //private ICategoriaRepository _categoriarepository = new CategoriaRepositorio();
        private ICategoriaRepository _categoriarepository;
        private ICategoriaQuery _categoriaquery;
        private IMediator _mediator;
        
        public CategoriasController(ICategoriaRepository categoriarepository, 
            ICategoriaQuery categoriaquery, IMediator mediator)
        {
            _categoriarepository = categoriarepository;
            _categoriaquery = categoriaquery;
            _mediator = mediator;
        }
       
        public ActionResult Index()
        {
            //var Categoria = _categoriarepository.ListCategoria();
            var categoria = _categoriarepository.ListCategoria();
            var model = new CategoriaIndexModel()
            {
                Titulo = "Primera prueba",
                Categorias = categoria
            };
            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevaCategoriaModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> New(NuevaCategoriaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "¡Categoria creada correctamente!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevaCategoriaModel()
                {
                    Nombre = model.Nombre,
                    Descripcion = model.Descripcion,
                };
                return View(modelReturn);
            }
            //if (!string.IsNullOrEmpty(model.Nombre))
            //{
            //    try
            //    {
            //        _categoriarepository.Add(new Dominio.Conocimientos.Categoria(model.Nombre, model.Descripcion));
            //        _categoriarepository.UnitOfWork.SaveChanges();
            //        TempData["success"] = "¡Nueva categoria creada correctamente!";
            //        return RedirectToAction("Index");
            //    }
            //    catch (Exception ex)
            //    {
            //        TempData["error"] = ex.Message;
            //        return View(model);
            //    }
            //}
            //else
            //{
            //    TempData["error"] = "Texto vacio, ingrese todos los datos solicitados.";
            //    return View(model);

            //}
        }
        public ActionResult Edit(int id)
        {
            var categoria = _categoriaquery.GetCategoria(id);

            var model = new NuevaCategoriaModel()
            {
                Nombre = categoria.Nombre,
                IdCategoria = id,
                Descripcion = categoria.Descripcion,
            };
            return View(model);
            //if (id <= 0)
            //{
            //    TempData["error"] = "Id no valido";
            //    return RedirectToAction("Index");
            //}
            //else
            //{
            //    var categoria = _categoriarepository.GetCategoria(id);

            //    var model = new NuevaCategoriaModel()
            //    {
            //        IdCategoria = id,
            //        Nombre = categoria.Nombre,
            //        Descripcion = categoria.Descripcion
            //    };

            //    return View(model);
            //}

        }
        [HttpPost] 
        public ActionResult Edit(NuevaCategoriaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var categoria = _categoriarepository.GetCategoria(model.IdCategoria);
                    categoria.Nombre = model.Nombre;
                    categoria.Descripcion = model.Descripcion;

                    _categoriarepository.Update(categoria);
                    _categoriarepository.UnitOfWork.SaveChanges();

                    TempData["success"] = "Categoria editada correctamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
        public ActionResult Delete(int id)
        {
            var categoria = _categoriarepository.GetCategoria(id);
            var model = new NuevaCategoriaModel()
            {
                Nombre = categoria.Nombre,
                IdCategoria = id,
                Descripcion = categoria.Descripcion
            };
            return View(model);

        }
        [HttpPost]
        public ActionResult Delete(NuevaCategoriaModel model)
        {
            try
            {
                var categoria = _categoriarepository.GetCategoria(model.IdCategoria);
                _categoriarepository.Delete(categoria);
                _categoriarepository.UnitOfWork.SaveChanges();

                TempData["success"] = "¡Categoria borrada exitosamente!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}