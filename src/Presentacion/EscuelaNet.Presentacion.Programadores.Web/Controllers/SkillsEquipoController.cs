﻿using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EsculaNet.Infraestructura.Programadores.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class SkillsEquipoController : Controller
    {
        private ISkillRepository Repositorio = new SkillsRepository();
        // GET: SkillsEquipo
        public ActionResult Index()
        {
            var skills = Repositorio.ListSkill();

            var model = new SkillsEquipoIndexModel()
            {
                Titulo = "Nuevo Conocimiento.",
                SkillsEquipo = skills
            };

            return View(model);
        }

        // GET: SkillsEquipo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SkillsEquipo/Create
        public ActionResult Create()
        {
            var model = new NuevoSkillsEquipoModel();
            return View(model);
        }

        // POST: SkillsEquipo/Create
        [HttpPost]
        public ActionResult Create(NuevoSkillsEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Descripcion))
            {
                try
                {
                    var conocimiento = new Skills(model.Descripcion, model.Grados);
                    Repositorio.Add(conocimiento);

                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Conocimiento Creado";  
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["Error"] = e.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }

        }
            // GET: SkillsEquipo/Edit/5
            public ActionResult Edit(int id)
            {
                if (id <= 0)
                {
                    TempData["error"] = "Id del Conocimiento no valido";
                    return RedirectToAction("Index");
                }
                var conocimiento = Repositorio.GetSkill(id);

                var model = new NuevoSkillsEquipoModel()
                {
                    Titulo = "Editar Conocimiento",
                    Descripcion = conocimiento.Descripcion,
                    Grados = conocimiento.Grados,
                    IdSkillsEquipo = id
                };
                return View(model);

            }

        // POST: SkillsEquipo/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoSkillsEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Descripcion))
            {
                try
                {
                    var skill = Repositorio.GetSkill(model.IdSkillsEquipo);
                    skill.Descripcion = model.Descripcion;
                    skill.Grados = model.Grados;

                    Repositorio.Update(skill);
                    Repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Conocimiento Modificado";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                    return View(model);

                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        // GET: SkillsEquipo/Delete/5
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id del Conocimiento no valido";
                return RedirectToAction("Index");
            }
            var skill = Repositorio.GetSkill(id);
            var model = new NuevoSkillsEquipoModel()
            {
                Titulo = "Borrar Conocimiento",
                Descripcion = skill.Descripcion,
                Grados = skill.Grados,
                IdSkillsEquipo = id
            };
            return View(model);
        }

        // POST: SkillsEquipo/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoSkillsEquipoModel model)
        {
            try
            {
                var conocimiento = Repositorio.GetSkill(model.IdSkillsEquipo);
                Repositorio.Delete(conocimiento);
                Repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Conocimiento borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
