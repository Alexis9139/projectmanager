﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.Repositorios;
using EscuelaNet.Presentacion.Proyectos.Web.Models;
using EscuelaNet.Dominio.Proyectos;

namespace EscuelaNet.Presentacion.Proyectos.Web.Controllers
{
    public class ProyectosController : Controller
    {
        private ILineaRepository _lineaRepository;
        public ProyectosController(ILineaRepository lineaRepository)
        {
            _lineaRepository = lineaRepository;
        }
        // GET: Proyectos
        public ActionResult Index(int id)
        {
            var linea = _lineaRepository.GetLinea(id);
            if(linea.Proyectos == null)
            {
                TempData["error"] = "Linea de Produccion sin proyectos.";
                return RedirectToAction("../Linea/Index");
            }
            var model = new ProyectoIndexModel()
            {
                Titulo = "Proyecto de la Linea de Produccion " + linea.Nombre,
                Proyectos = linea.Proyectos.ToList(),
                Linea = id
            };

            return View(model);
        }

        public ActionResult New(int id)
        {
            var linea = _lineaRepository.GetLinea(id);
            var model = new NuevoProyectoModel() {
                IDLinea = id
            };
            return View(model);
        }

        [HttpPost]

        public ActionResult New(NuevoProyectoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var nombre = model.Nombre;
                    var descripcion = model.Descripcion;
                    var nombreResponsable = model.NombreResponsable;
                    var emailResponsable = model.EmailResponsable;
                    var telefonoResponsable = model.TelefonoResponsable;

                    var linea = _lineaRepository.GetLinea(model.IDLinea);
                    linea.pushProyecto(nombre, descripcion, nombreResponsable, telefonoResponsable, emailResponsable);
                    _lineaRepository.Update(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Proyecto creado";
                    return RedirectToAction("Index/"+model.IDLinea);
                }catch(Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int idProy, int idLinea)
        {

            var proyecto = _lineaRepository.GetProyecto(idProy);
            var model = new NuevoProyectoModel()
            {
                Nombre = proyecto.Nombre,
                Id = idProy,
                Descripcion = proyecto.Descripcion,
                NombreResponsable = proyecto.NombreResponsable,
                TelefonoResponsable = proyecto.TelefonoResponsable,
                EmailResponsable = proyecto.EmailResponsable,
                IDLinea = proyecto.IDLinea
            };

            return View(model);
        }

        [HttpPost]

        public ActionResult Edit(NuevoProyectoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre) || !string.IsNullOrEmpty(model.Descripcion))
            {
                try
                {
                    var linea = _lineaRepository.GetLinea(model.IDLinea);
                    linea.Proyectos.Where(proy => proy.ID == model.Id).First().Nombre = model.Nombre;
                    linea.Proyectos.Where(proy => proy.ID == model.Id).First().Descripcion = model.Descripcion;
                    linea.Proyectos.Where(proy => proy.ID == model.Id).First().NombreResponsable = model.NombreResponsable;
                    linea.Proyectos.Where(proy => proy.ID == model.Id).First().TelefonoResponsable = model.TelefonoResponsable;
                    linea.Proyectos.Where(proy => proy.ID == model.Id).First().EmailResponsable = model.EmailResponsable;

                    _lineaRepository.Update(linea);
                    _lineaRepository.UnitOfWork.SaveChanges();
                    TempData["success"] = "Proyecto modificado";
                    return RedirectToAction("Index", new { id= model.IDLinea });
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Se pasó un nombre o descripcion vacio/erroneo.";
                return View(model);
            }
        }

        public ActionResult Delete(int idLinea, int idProy)
        {
            var linea = _lineaRepository.GetLinea(idLinea);
            var proyecto = _lineaRepository.GetProyecto(idProy);
            var model = new NuevoProyectoModel()
            {
                Nombre = proyecto.Nombre,
                Id = idProy
            };
            return View(model);

        }


        [HttpPost]

        public ActionResult Delete(NuevoProyectoModel model)
        {
            try
            {
                var proyecto = _lineaRepository.GetProyecto(model.Id);
                _lineaRepository.DeleteProyecto(proyecto);
                _lineaRepository.UnitOfWork.SaveChanges();
                TempData["success"] = "Proyecto eliminado";
                return RedirectToAction("Index", new { id = model.IDLinea});
            }
            catch(Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

    }
}