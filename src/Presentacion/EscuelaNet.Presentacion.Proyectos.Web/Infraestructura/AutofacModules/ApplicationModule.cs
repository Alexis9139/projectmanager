﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Autofac;
using EscuelaNet.Aplicacion.Proyectos.QueryServices.LineaServices;
using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Infraestructura.Proyectos;
using EscuelaNet.Infraestructura.Proyectos.Repositorios;

namespace EscuelaNet.Presentacion.Proyectos.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var connectionString =
                       ConfigurationManager
                           .ConnectionStrings["LineaContext"].ToString();
            builder.Register(c => new LineaQuery(
                            connectionString))
                        .As<ILineaQuery>().InstancePerLifetimeScope();
            builder.RegisterType<LineaContext>()
                .InstancePerRequest();
            builder.RegisterType<LineaRepository>()
                .As<ILineaRepository>()
                .InstancePerLifetimeScope();
            builder.RegisterType<TecnologiaRepository>()
                .As<ITecnologiaRepository>()
                .InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}